from flask import Flask, render_template, request

app = Flask(__name__)

@app.route("/", methods=["GET", "POST"])
def home():
    temperature = request.form.get('temperature')
    mode = request.form.get('mode')
    my_context = {
        "value": TemperatureConverter(mode, temperature).get_temperature(),
        "mode": mode
    }
    return render_template("home.html", context=my_context)


class TemperatureConverter:

    def __init__(self, mode, value):
        self.mode = mode
        self.value = value

    def validated_value(self):
        try:
            return float(self.value)
        except ValueError:
            return None

    def get_temperature(self):
        if self.mode == 'Kelwin' and self.value:
            value = self.validated_value()
            if value:
                return value + 273.15
        elif self.mode == 'Fahrenheit' and self.value:
            value = self.validated_value()
            if value:
                return value * 9/5 + 32
        return None